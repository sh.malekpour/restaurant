import React, { useState } from "react";
import { withStyles, Icon } from "@material-ui/core";
import classNames from "classnames";
const styles = {
  root: {
    border: " 1px solid #ccc",
  },
  addresebtn: {
    borderRight: "0",
    borderTopRightRadius: "0px",
    borderBottomRightRadius: "0px",
  },
  cartBtn: {
    borderTopLeftRadius: "0",
    borderBottomLeftRadius: "0",
    background: "rgb(255, 224, 51)",
    borderColor: "rgb(255, 224, 51)",
  },
  hidden: {
    display: "none",
  },
};
const HeaderAddressBtn = (props) => {
  const [changeBtn, setChangeBtn] = useState(false);
  const toggleButton = () => {
    setChangeBtn(!changeBtn);
  };
  return (
    <>
      <button
        className={classNames(
          "border py-1 xs-block px-4 rounded-full ml-5 focus:outline-none ",
          props.classes.root,
          changeBtn ? props.classes.addresebtn : null
        )}
        onClick={toggleButton}
      >
        <Icon color="inherit" style={{ fontSize: 18, paddingTop: "0.10rem" }}>
          near_me
        </Icon>
        <span> address button</span>
      </button>
      <button
        className={classNames(
          "border py-1 xs-block px-4 rounded-full focus:outline-none ",
          props.classes.root,
          changeBtn ? props.classes.cartBtn : props.classes.hidden
        )}
      >
        <Icon color="inherit" style={{ fontSize: 18, paddingTop: "0.10rem" }}>
          local_grocery_store
        </Icon>
        <span>add</span>
      </button>
    </>
  );
};
export default withStyles(styles)(HeaderAddressBtn);
