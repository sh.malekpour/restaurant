import React from "react";
import HeaderLogo from "./HeaderLogo";
import HeaderButton from "./HeaderButton";
import { Link } from "react-router-dom";
import { Box, Icon, withStyles } from "@material-ui/core";
import HeaderAddressBtn from "./HeaderAddressBtn";
import classNames from "classnames";

const links = [
  { name: "Link1", address: "/1", id: 1 },
  { name: "Link2", address: "/2", id: 2 },
  { name: "Link3", address: "/3", id: 3 },
];
const buttons = [
  { name: "button1", id: 1 },
  { name: "button2", id: 2 },
];
const styles = {
  root: {
    lineHeight: "25px",
  },
};
const ToolBarContent = (props) => {
  return (
    <div className=" md:mx-2 w-full text-base my-4">
      <div className=" flex flex-row justify-between">
        <Box className="block mx-auto md:mx-0">
          <HeaderLogo />
        </Box>
        <Box className="hidden md:block leading-loose">
          {links.map((link, index) => (
            <Link key={index} to={link.address} className="mx-5">
              {link.name}
            </Link>
          ))}
        </Box>
        <Box className={classNames("hidden md:block", props.classes.root)}>
          <HeaderAddressBtn />
          {buttons.map((button, index) => (
            <HeaderButton key={index} textButton={button.name} />
          ))}
        </Box>
        <Box className="flex md:hidden">
          <Icon style={{ fontSize: 29, color: "#e5e5e5" }}>access_time</Icon>
        </Box>
      </div>
    </div>
  );
};

export default withStyles(styles)(ToolBarContent);
