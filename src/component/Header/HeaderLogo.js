import React from "react";
const myLogo = {
  src: "./logo.svg",
  alt: "logo",
};
const Logo = () => {
  return (
    <>
      <img
        src={myLogo.src}
        alt={myLogo.alt}
        className="w-30 sm:w-40"
      />
    </>
  );
};
export default Logo;
