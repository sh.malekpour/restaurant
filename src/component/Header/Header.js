import React from "react";
import clsx from "clsx";
import {
  makeStyles,
  CssBaseline,
  AppBar,
  IconButton,
  Toolbar,
  SwipeableDrawer,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import HeaderDrawer from "./HeaderDrawer";
import HeaderContent from "./HeaderContent";
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    color: "black",
    background: "white",
    boxShadow: " 0 1px 0 0 rgba(0,0,0,.05)",
  },
  menuButton: {
    display: "none",
    [theme.breakpoints.down("sm")]: {
      display: "block",
    },
  },
  hide: {
    display: "none",
  },
}));
const Header = () => {
  const classes = useStyles();
  const [state, setState] = React.useState({
    left: false,
  });
  const anchor = "left";
  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };
  const [open] = React.useState(false);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={toggleDrawer(anchor, true)}
            edge="start"
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <HeaderContent />
        </Toolbar>
      </AppBar>
      <SwipeableDrawer
        anchor={anchor}
        open={state[anchor]}
        onClose={toggleDrawer(anchor, false)}
        onOpen={toggleDrawer(anchor, true)}
      >
        <HeaderDrawer toggleDrawer={toggleDrawer(anchor, false)} />
      </SwipeableDrawer>
    </div>
  );
};
export default Header;
