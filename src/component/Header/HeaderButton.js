import React from "react";
import { withStyles } from "@material-ui/core/styles";
import classNames from "classnames";
const styles = {
  root: {
    border: " 1px solid #ccc",
  },
};
const HeaderButton = (props) => {
  return (
    <button
      className={classNames(
        "border py-1 xs-block px-4 rounded-full ml-5 focus:outline-none ",
        props.classes.root
      )}
    >
      {props.textButton}
    </button>
  );
};

export default withStyles(styles)(HeaderButton);
