import React from "react";
import { Switch, Route } from "react-router-dom";
import "./App.css";
import Header from "./component/Header/Header";
function App() {
  return (
    <>
      <Header/>
      <Switch>
        <Route path="/" exact component="" />
      </Switch>
    </>
  );
}

export default App;
